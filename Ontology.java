import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import openllet.owlapi.OpenlletReasoner;
import openllet.owlapi.OpenlletReasonerFactory;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.*;
import org.semanticweb.owlapi.model.parameters.ChangeApplied;
import org.semanticweb.owlapi.reasoner.Node;
import org.semanticweb.owlapi.reasoner.NodeSet;
import org.semanticweb.owlapi.search.EntitySearcher;
import org.semanticweb.owlapi.util.DefaultPrefixManager;
import org.semanticweb.owlapi.vocab.OWL2Datatype;
import org.semanticweb.owlapi.vocab.OWLFacet;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.*;
import java.util.stream.Stream;

public class Ontology {

//    private final String IS_CURED_BY = "is_cured_by";
//    private final String IS_DIAGNOSED_BY = "is_diagnosed_by";
//
//    private final String hasJobField = "has_job_field";
//    private final String hasWorkingTime = "has_working_time";
//    private final String hasEducationField = "has_education_field";
//    private final String hasGrade = "has_grade";
//    private final String hasCompetency = "has_competency";

    public OWLOntologyManager manager;
    public OWLDataFactory factory;
    public String prefix;
    public String NS;
    public OWLOntology ont;
    //    OWLReasonerFactory reasonerFactory;
    public OpenlletReasoner reasoner;
//    String SYMPTOMS = "Symptoms_of_diseases";
//    private String HEARTfAILURE = "Congenital_heart_disease";
//    private final String IS_RELATED_WITH = "is_related_with";
//    OWLClass maxCls;
//    private ArrayList<String> treatmentList = new ArrayList<>();
//    private ArrayList<String> diagnoseList = new ArrayList<>();


    public Ontology(String path) throws OWLOntologyCreationException, URISyntaxException {

        this.manager = OWLManager.createOWLOntologyManager();
        this.factory = manager.getOWLDataFactory();
        //    System.out.println(Resources.getResource("Congenital_heart_defect_3_clean.owl"));
        IRI ontologyIRI = IRI.create(new File(path));

        ont = load(ontologyIRI);
        // ont.toString();
        reasoner = createReasoner(ont);

        prefix = ont.getOntologyID().getOntologyIRI().get().toString()+"#";
        System.out.println(prefix);
        NS = ont.getOntologyID().getOntologyIRI().get().toString();
        // System.out.println("namespace: " + NS);


    }


//    public void mytest() throws OWLOntologyCreationException {
//        try (final OWLManagerGroup group = new OWLManagerGroup())
//        {
//            final OWLOntologyID ontId = OWLHelper.getVersion(IRI.create("http://myOnotology"), 1.0);
//            final OWLHelper owl = new OWLGenericTools(group, ontId, true);
//
//            final OWLNamedIndividual x1 = OWL.Individual("#I1");
//            final OWLNamedIndividual x2 = OWL.Individual("#I2");
//
//            owl.addAxiom(OWL.equivalentClasses(ClsA, OWL.some(propB, OWL.restrict(XSD.STRING, OWL.facetRestriction(OWLFacet.PATTERN, OWL.constant("A.A"))))));
//            owl.addAxiom(OWL.propertyAssertion(x1, propB, OWL.constant("AAA")));
//            owl.addAxiom(OWL.propertyAssertion(x2, propB, OWL.constant("BBB")));
//            owl.addAxiom(OWL.differentFrom(x1, x2));
//
//            final OpenlletReasoner r = (OpenlletReasoner) owl.getReasoner();
//            assertTrue(r.isEntailed(OWL.classAssertion(x1, ClsA)));
//            assertFalse(r.isEntailed(OWL.classAssertion(x2, ClsA)));
//        }
//    }


    public ChangeApplied assignObjectPropertyToIndividual(String individualFragment, String obj, String fillerIndividual) {

        if (individualFragment.length() <= 0) return null;

        OWLNamedIndividual individual = factory.getOWLNamedIndividual(individualFragment, prefix);
        OWLNamedIndividual fillerInd = factory.getOWLNamedIndividual(fillerIndividual, prefix);
        OWLObjectProperty objectProperty = factory.getOWLObjectProperty(obj, prefix);
        // To specify that :John is related to :Mary via the :hasWife property
        // we create an object property assertion and add it to the ontology
        OWLObjectPropertyAssertionAxiom propertyAssertion =
                factory.getOWLObjectPropertyAssertionAxiom(objectProperty, individual, fillerInd);
       return manager.addAxiom(ont, propertyAssertion);

       // AddAxiom addAx = new AddAxiom(ont, propertyAssertion);
      //  return manager.applyChange(addAx);
        //  manager.saveOntology(ont);
    }


    public ChangeApplied assignObjectPropertyToIndividual(OWLIndividual individualLeft, String obj, OWLIndividual fillerIndividual) {
        PrefixManager pm = new DefaultPrefixManager(null, null, ont.getOntologyID().getOntologyIRI().toString());
        OWLObjectProperty objectProperty = factory.getOWLObjectProperty(prefix + obj);
        System.out.println(objectProperty.getIRI());
        OWLObjectPropertyAssertionAxiom propertyAssertion =
                factory.getOWLObjectPropertyAssertionAxiom(objectProperty, individualLeft, fillerIndividual);
       return manager.addAxiom(ont, propertyAssertion);

       // AddAxiom addAx = new AddAxiom(ont, propertyAssertion);
      //  return manager.applyChange(addAx);
        //  manager.saveOntology(ont);
    }


    public ChangeApplied assignDataPropertyToIndividual(OWLIndividual individualLeft, String obj, String filler) {
        //  PrefixManager pm = new DefaultPrefixManager(null, null, ont.getOntologyID().getOntologyIRI().toString());
        OWLDataProperty dataProperty = factory.getOWLDataProperty(prefix + obj);
        OWLDataPropertyAssertionAxiom propertyAssertion = factory.getOWLDataPropertyAssertionAxiom(dataProperty, individualLeft, filler);
        manager.addAxiom(ont, propertyAssertion);

        AddAxiom addAx = new AddAxiom(ont, propertyAssertion);
        return manager.applyChange(addAx);
        //  manager.saveOntology(ont);
    }

    public void saveOntology(OWLOntology ont) throws OWLOntologyStorageException {
        manager.saveOntology(ont);
    }

    public ChangeApplied addObjectPropertyValue(String cls, String obj, String filler) {

        if (filler.length() <= 0) return null;

        OWLClass aClass = getClassByFragment(cls);
        OWLClass fillerClass = getClassByFragment(filler);
        OWLObjectProperty hasEducationField = factory.getOWLObjectProperty(IRI.create(prefix, obj));

        OWLObjectSomeValuesFrom ed = factory.getOWLObjectSomeValuesFrom(hasEducationField, fillerClass);
        OWLSubClassOfAxiom ax = factory.getOWLSubClassOfAxiom(aClass, ed);
        AddAxiom addAx = new AddAxiom(ont, ax);
        return manager.applyChange(addAx);
    }


    public ChangeApplied addDataPropertyValue(String cls, String dataPropertyFragment, String filler) {

        OWLClass aClass = getClassByFragment(cls);
        // OWLClass fillerClass = getClassByFragment(filler);
        OWLDataProperty hasEducationField = factory.getOWLDataProperty(IRI.create(prefix, dataPropertyFragment));

        OWLDataSomeValuesFrom ed = factory.getOWLDataSomeValuesFrom(hasEducationField, OWL2Datatype.valueOf(filler));
        OWLSubClassOfAxiom ax = factory.getOWLSubClassOfAxiom(aClass, ed);
        AddAxiom addAx = new AddAxiom(ont, ax);
        return manager.applyChange(addAx);
    }


    public ChangeApplied addDataPropertyStrictValueToClass(String clsFragment, String dataPropertyFragment, String filler) throws NoSuchFieldException {
        System.out.println(filler);
        OWLDataProperty dataProperty = factory.getOWLDataProperty(IRI.create(prefix, dataPropertyFragment));
        OWLClass aClass = getClassByFragment(clsFragment);

        OWLFacetRestriction maxInclusive = factory.getOWLFacetRestriction(OWLFacet.MAX_INCLUSIVE, Integer.parseInt(filler));
        OWLFacetRestriction minInclusive = factory.getOWLFacetRestriction(OWLFacet.MIN_INCLUSIVE, Integer.parseInt(filler));
        ArrayList<OWLFacetRestriction> strictFacets = new ArrayList<OWLFacetRestriction>();

        strictFacets.add(maxInclusive);
        strictFacets.add(minInclusive);

        OWLDatatypeRestriction datatypeRestriction = factory.getOWLDatatypeRestriction(factory.getIntegerOWLDatatype(), strictFacets);


        OWLClassExpression dataPropertyDefinition = factory.getOWLDataSomeValuesFrom(dataProperty, datatypeRestriction);

        OWLSubClassOfAxiom ax = factory.getOWLSubClassOfAxiom(aClass, dataPropertyDefinition);
        return manager.applyChange(new AddAxiom(ont, ax));
    }


    public ChangeApplied addDataPropertyStrictHasValueToClass(String clsFragment, String dataPropertyFragment, String filler) {
        OWLDataFactory df = manager.getOWLDataFactory();
        // OWLDataProperty hasAge = df.getOWLDataProperty(IRI.create(EXAMPLE_IRI + "hasAge"));
        OWLDataProperty dataProperty = factory.getOWLDataProperty(IRI.create(prefix, dataPropertyFragment));
        //  OWLClass adult = df.getOWLClass(IRI.create(EXAMPLE_IRI + "Adult"));
        OWLClass aClass = getClassByFragment(clsFragment);

        OWLAxiom ax = df.getOWLSubClassOfAxiom(aClass, df.getOWLDataHasValue(dataProperty, df.getOWLLiteral(Integer.parseInt(filler))));
        System.out.println(ax);
        System.out.println("---------------");

        return manager.applyChange(new AddAxiom(ont, ax));

        // OWLOntology ont = m.createOntology();
        //   ont.add(ax);
        // ont.saveOntology(new ManchesterSyntaxDocumentFormat(), System.out);
    }


    OWLOntology load(IRI path) throws OWLOntologyCreationException {
        // in this test, the ontology is loaded from a string
        return manager.loadOntologyFromOntologyDocument(path);
    }

    OWLOntology load(InputStream stream) throws OWLOntologyCreationException {
        // in this test, the ontology is loaded from a string
        return manager.loadOntologyFromOntologyDocument(stream);
    }

    OWLOntology load(File file) throws OWLOntologyCreationException {
        // in this test, the ontology is loaded from a string
        return manager.loadOntologyFromOntologyDocument(file);
    }


    public OWLIndividual addIndividualToClass(String classFragment, String individualFragment) throws OWLOntologyStorageException {
        //OWLClass cls = getClassByFragment(classFragment);
//        AtomicBoolean flag = new AtomicBoolean(false);
        PrefixManager pm = new DefaultPrefixManager(NS); // assuming NS is string and is your namespace
        OWLClass tClass = factory.getOWLClass(classFragment, pm); //assuming '#' is your delimiter
        System.out.println(tClass.getIRI().toString());
        OWLIndividual tIndividual = factory.getOWLNamedIndividual(individualFragment, pm);

        //  System.out.println("amin: "+tIndividual);
//        OWLNamedIndividual aa = factory.getOWLNamedIndividual("dddddd");

//        System.out.println("start");

//        EntitySearcher.getTypes(tIndividual, ont).forEach(x -> {
//
//                    if (x == null) {
//                        OWLClassAssertionAxiom classAssertion = factory.getOWLClassAssertionAxiom(tClass, tIndividual);
//                        manager.addAxiom(ont, classAssertion);
//                        try {
//                            manager.saveOntology(ont);
//                            //  flag.set(true);
//                        } catch (OWLOntologyStorageException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }

        //  );

        OWLClassAssertionAxiom classAssertion = factory.getOWLClassAssertionAxiom(tClass, tIndividual);
        manager.addAxiom(ont, classAssertion);
        try {
            manager.saveOntology(ont);
            //  flag.set(true);
        } catch (OWLOntologyStorageException e) {
            e.printStackTrace();
        }


        //return flag.get();
        return tIndividual;

//        System.out.println("end");


        //  tIndividual.datatypesInSignature().findAny().ifPresent(x-> System.out.println(x.getIRI().getFragment()));
        //tIndividual.getIndividualsInSignature().stream().findFirst().ifPresent(x-> System.out.println(x.containsEntityInSignature()));

//        ont.individualsInSignature().forEach(p->{
//            Set<OWLClass> types = reasoner.types(p).collect(Collectors.toSet());
//            types.stream().forEach(x->{
//               // System.out.println(x.getIRI().getFragment());
//                if(x.getIRI().getFragment().equals("Thing")) {System.out.println("Types = " + types+" ind: "+ p.getIRI().getFragment());}
//
//            });
//        });


    }

//    NodeSet<OWLClass> getSubClassesAsNodeSet(String cls,boolean onlyFragment) {
//        // System.out.println(factory.getOWLClass(IRI.create(prefix, cls)));
//        NodeSet<OWLClass> subClasses = reasoner.getSubClasses(factory.getOWLClass(IRI.create(prefix, cls)));
//        // System.out.println("symptomsClass = " + symptomsClass);
//        return subClasses;
//    }

    ArrayList<String> getSubClassesAsArrayList(String cls, boolean onlyFragment, boolean direct) {
        ArrayList<String> myArray = new ArrayList<>();
        NodeSet<OWLClass> subClasses = reasoner.getSubClasses(factory.getOWLClass(IRI.create(prefix, cls)), direct);
        // NodeSet<OWLClass> subClasses = getSubClassesAsNodeSet(cls,onlyFragment);

        for (Node<OWLClass> mycls : subClasses) {
            if (onlyFragment) {
                myArray.add(mycls.getEntities().stream().findFirst().get().getIRI().getFragment());

            } else {
                myArray.add(mycls.getEntities().stream().findFirst().get().toStringID());
            }
        }
        return myArray;
    }


    private OpenlletReasoner createReasoner(OWLOntology rootOntology) {
        return OpenlletReasonerFactory.getInstance().createReasoner(rootOntology);
    }


    public String getLabel(Node<OWLClass> cls) {
        OWLAnnotationValue label = null;
        Optional<OWLAnnotation> la = EntitySearcher.getAnnotations(cls.getRepresentativeElement(), ont, factory.getRDFSLabel()).findFirst();
        if (la.isPresent()) label = la.get().getValue();
        if (label instanceof OWLLiteral) {
            return ((OWLLiteral) label).getLiteral().toString();
        } else {
            return cls.getRepresentativeElement().getIRI().getFragment();
        }

    }

    public String getLabel(OWLClass cls) {
        OWLAnnotationValue label = null;
        Optional<OWLAnnotation> la = EntitySearcher.getAnnotations(cls, ont, factory.getRDFSLabel()).findFirst();
        if (la.isPresent()) label = la.get().getValue();
        if (label instanceof OWLLiteral) {
            return ((OWLLiteral) label).getLiteral().toString();
        } else {
            return cls.getIRI().getFragment();
        }

    }

    public OWLClass getClassByFragment(String clsFragment) {
        return factory.getOWLClass(IRI.create(prefix, clsFragment));
    }

    public boolean isSubClassOf(OWLClass subCls, OWLClass superCls) {
        String sup = superCls.getIRI().toString();
        Iterator<Node<OWLClass>> ite = reasoner.getSuperClasses(subCls).iterator();

        while (ite.hasNext()) {
            String ne = ite.next().entities().findFirst().get().getIRI().toString();
            if (ne.equals(sup)) return true;
        }
        return false;
    }

    public boolean isSubClassOf(String subClsFragment, String superClsFragment) {

        OWLClass sub = getClassByFragment(subClsFragment);
        String sup = getClassByFragment(superClsFragment).getIRI().toString();

        for (Node<OWLClass> owlClasses : reasoner.getSuperClasses(sub)) {
            String ne = owlClasses.entities().findFirst().get().getIRI().toString();
            System.out.println(ne);
            if (ne.equals(sup)) return true;
        }
        return false;
    }


    public static void main(String[] args) throws OWLOntologyCreationException, URISyntaxException {
        String path = "elearning2_old.owl";
        String user = "user";
        String jobField = "moaven";
        String COMPETENCY_FRAGMENT = "competency";
        String HAS_JOB_FIELD = "has_job_field";
        ArrayList<String> relatedCompetencies = new ArrayList();
        Ontology ontl = new Ontology(path);
        OWLClass cls = ontl.factory.getOWLClass(IRI.create(ontl.prefix, user));
        ArrayList<String> userSelectedCompetencies = new ArrayList<>();
        ArrayList<String> userSelectedSubCompetencies = new ArrayList<>();
        final Multimap<String, String> finalCompWithSubCompMultimap = ArrayListMultimap.create();
        userSelectedCompetencies.add("tafakor_tahlili");
        userSelectedSubCompetencies.add("اصول_کار_تیمی_و_مشارکتی");

        //get  all COMPETENCY_FRAGMENT classes
        OWLClass competencyCls = ontl.getClassByFragment(COMPETENCY_FRAGMENT);
        Set<OWLClass> competencyDirect = ontl.reasoner.getSubClasses(competencyCls, true).getFlattened();
        //filter classes by obj property
        RestrictionVisitorForCompetency visitor = new RestrictionVisitorForCompetency(Collections.singleton(ontl.ont), HAS_JOB_FIELD, jobField);

        for (OWLClass ocls : competencyDirect) {
            ocls.accept(visitor);
            //  relatedCompetencies.addAll(visitor.getRelatedCompetencyClasses());
            for (Object one : visitor.getRelatedClasses()) {
                if (!relatedCompetencies.contains(one)) relatedCompetencies.add(one.toString());
            }
        }

        //check and discard filtered result matched by user selection
        relatedCompetencies.removeAll(userSelectedCompetencies);
        //get sub Competencies of related competencies
        relatedCompetencies.forEach(x -> {
            Set<OWLClass> cSubs = ontl.reasoner.getSubClasses(ontl.getClassByFragment(x), true).getFlattened();
            ArrayList<String> selectedSubs = new ArrayList<>();
            //check and discard filtered result's sub competencies by user selected sub competencies

            cSubs.forEach(w -> {
                String fr = w.getIRI().getFragment();
                if (!userSelectedSubCompetencies.contains(fr)) //selectedSubs.add(fr);//cSubs.remove(w);
                    // add cleaned competencies and their sub competencies to a multimap
                    finalCompWithSubCompMultimap.put(x, fr);
            });
        });

        for (Map.Entry<String, Collection<String>> entry : finalCompWithSubCompMultimap.asMap().entrySet()) {
            String key = entry.getKey();
            Collection<String> value = entry.getValue();
            System.out.println(key + ":" + value);
        }
    }


    public ArrayList getClassIndividuals(String clsFragment) {
        ArrayList<String> ind = new ArrayList<String>();
        OWLClass cls = getClassByFragment(clsFragment);
//        System.out.println("CLS: " + cls.getIRI());
        NodeSet<OWLNamedIndividual> indListReasoned = reasoner.getInstances(cls);

       // Stream<OWLIndividual> indList = EntitySearcher.getIndividuals(cls, ont);

        indListReasoned.forEach(x -> {
//              System.out.println("UUU:"+x.asOWLNamedIndividual().getIRI());
            String dd = x.entities().findFirst().get().getIRI().getFragment();
          //  String dd = x.getIndividualsInSignature().stream().findFirst().get().getIRI().getFragment();
           // System.out.println(dd);
            ind.add(dd);
        });
        return ind;
    }

    /**
     * Visits existential restrictions and collects the properties which are
     * restricted
     */
    public static class RestrictionVisitorForCompetency extends OWLClassExpressionVisitorAdapter {

        @Nonnull
        private final Set<OWLClass> processedClasses;
        @Nonnull
        private final Set<OWLObjectPropertyExpression> restrictedProperties;
        private final Set<OWLOntology> onts;
        private final Multimap<String, String> m = ArrayListMultimap.create();
        private final ArrayList<String> myArray = new ArrayList<>();
        private final String objPropName;
        private OWLClass mClass;
        private String fillerVal;

        public RestrictionVisitorForCompetency(Set<OWLOntology> onts, String objectPropertyRestrictionName, String filler) {
            restrictedProperties = new HashSet<>();
            processedClasses = new HashSet<>();
            this.onts = onts;
            objPropName = objectPropertyRestrictionName;
            fillerVal = filler;
        }

        @Nonnull
        public Set<OWLObjectPropertyExpression> getRestrictedProperties() {
            return restrictedProperties;
        }

        public Multimap getVisitorObjectPropertySomeValueFrom() {
            return m;
        }

        public ArrayList getRelatedClasses() {
            return myArray;
        }

        @Override
        public void visit(OWLClass ce) {
            // avoid cycles
            if (!processedClasses.contains(ce)) {
                // If we are processing inherited restrictions then
                // we recursively visit named supers.
                processedClasses.add(ce);

                for (OWLOntology ont : onts) {
                    for (OWLSubClassOfAxiom ax : ont.getSubClassAxiomsForSubClass(ce)) {
                        mClass = ce;

                        //   System.out.println("inside class:"+ce.getIRI().toString());
                        ax.getSuperClass().accept(this);
                    }
                }
            }
        }

//        @Override
//        public void visit(@Nonnull OWLObjectSomeValuesFrom ce) {
//            if (!objPropName.isEmpty() && !ce.getProperty().getNamedProperty().toStringID().contains(objPropName))
//                return;
//            ce.getFiller().signature().forEach(x -> {
//                m.put(ce.getProperty().getNamedProperty().getIRI().toString(), x.getIRI().toString());
//            });
//        }

        @Override
        public void visit(@Nonnull OWLObjectSomeValuesFrom ce) {
            if (!objPropName.isEmpty() && !ce.getProperty().getNamedProperty().toStringID().contains(objPropName))
                return;


            ce.getFiller().signature().forEach(x -> {
                //  m.put(ce.getProperty().getNamedProperty().getIRI().toString(), x.getIRI().toString());
                //  System.out.println("obj:"+x.toStringID());
                if (x.toStringID().contains(fillerVal)) {
                    myArray.add(mClass.getIRI().getFragment());
                    // myArray.forEach(xe -> System.out.println(xe));
                    //    System.out.println("inside object visitor:"+ce.getProperty().getNamedProperty().toStringID()+mClass.getIRI().toString());
                    mClass = null;
                }
            });
        }
    }


}


